--Project Euler 48

import Data.Char
import Data.List 
import Test.QuickCheck

series :: String
series = reverse(take 10 (reverse(show(sum[ x^x | x <- [1..1000]]))))