--Project Euler 20

import Data.Char
import Data.List 
import Test.QuickCheck

fac :: Integer -> Integer
fac 0 = 1
fac x = x * fac (x-1)
		   
sumFac :: Int
sumFac = foldr (+) 0 (map digitToInt(show(fac 100)))


