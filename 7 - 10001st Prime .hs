--Project Euler 7

import Data.Char
import Data.List 

--inefficient for large numbers 
isPrime :: Int -> Bool
isPrime n = and [ n `mod` z /= 0 | z <- [1..(n `div` 2)], z /= 1, z /= n]

prime :: Int -> Int
prime x = ([2,3] ++ [ y | y <- [5,7,9..], isPrime y]) !! x




