--Project Euler 16

import Data.Char
import Data.List 
import Test.QuickCheck


sumPower :: Int
sumPower = sum(map digitToInt (show (2^1000)))
