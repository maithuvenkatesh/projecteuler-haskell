--Project Euler 2

import Data.Char
import Data.List 

sumFib :: Int
sumFib = sum [ x | x <- fib [2,1], even x]


fib :: [Int] -> [Int]
fib [] = []
fib xs | head xs <= 4000000 = fib ((xs !! 0 + xs !! 1):xs)
	   | otherwise = xs
	   
fibs = 1 : 1 : zipWith (+) fibs (tail fibs)

