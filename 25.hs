--Project Euler 25

import Data.Char
import Data.List

fib :: [Int] -> [Int]
fib [] = []
fib xs | check [2,1] == 1000 = fib ((xs !! 1 + xs !! 0):xs)

check :: [Int] -> Int
check xs = length(map intToDigit (fib xs))

termFib :: Int -> Int
termFib x = (fib [2,1]) !! x
