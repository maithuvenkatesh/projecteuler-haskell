--Project Euler 10

import Data.Char
import Data.List 

isPrime :: Int -> Bool
isPrime n = and [ n `mod` z /= 0 | z <- [1..n], z /= 1, z /= n, odd z]

sumPrime :: Int -> Int
sumPrime x = sum[ y | y <- [10..x], y /= x, odd y, isPrime y] + 2 + 3 + 5 + 7		  
