--Problem 9

import Data.Char
import Data.List

pythagoras :: [Int]
pythagoras = pythagoreanTriplet[(x,y,z) | x <- [1..997], y <- [2..998], z <- [3..1000], x + y + z == 1000, x/= y, y /= z]

pythagoreanTriplet :: [(Int,Int,Int)] -> [Int]
pythagoreanTriplet ((x,y,z):xs) | (x*x + y*y == z*z) = (x*y*z) : pythagoreanTriplet xs
							    | otherwise = pythagoreanTriplet xs