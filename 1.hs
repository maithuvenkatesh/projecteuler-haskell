--Project Euler 1

import Data.Char
import Data.List 
import Test.QuickCheck

checkMultiple :: Int -> Bool
checkMultiple x | x `mod` 3 == 0 || x `mod` 5 == 0 = True
				| otherwise = False

sumMultiple :: Int 
sumMultiple = sum[ x | x <- [0..999], checkMultiple x]

one :: Integer -> Integer -> Integer
one x y | x <= 1000 = one(x + 1 y+test x)
		| otherwise = y

test :: Integer -> Integer
test x | x `mod` 3 == 0 = x
	   | x `mod` 5 == 0 = x
	   | otherwise = 0