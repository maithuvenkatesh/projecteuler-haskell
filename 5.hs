--Project Euler 5

import Data.Char
import Data.List 

divisible :: Int -> Bool
divisible x = and [ x `mod` n == 0 | n <- [1..20]]

smallestNumber :: [Int]
smallestNumber =  [i | i <- [1000..], divisible i]
