--Problem 8

import Data.Char
import Data.List

greatestProduct :: [Int] -> [Int]
greatestProduct [] = []
greatestProduct (a:b:c:d:e:xs) = a*b*c*d*e : greatestProduct xs

inputFile :: String -> [Int]
inputFile xs = map digitToInt xs

outputFile :: [Int] -> String
outputFile xs = map chr (greatestProduct xs)

main = do
	  myFile <- readFile "problem8.txt"
	  putStrLn ((outputFile.inputFile) myFile)
	  

	   
	   
	  
				
