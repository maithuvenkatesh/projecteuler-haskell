--Project Euler 3

import Data.Char
import Data.List 
import Test.QuickCheck

largestPrimeFactor :: [Int]
largestPrimeFactor = [ y | y <- [1..], isPrime y, 600851475143 `mod` y == 0]

isPrime :: Int -> Bool
isPrime n = and [ n `mod` z /= 0 | z <- [1..n], z /= 1, z /= n]
		  
