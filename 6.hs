--Project Euler 6

import Data.Char
import Data.List 
import Test.QuickCheck

sumSquares :: Int
sumSquares = sum[ x * x | x <- [1..100]]

squareSum :: Int
squareSum = (sum [1..100])^2

difference :: Int 
difference = squareSum - sumSquares

