--Problem 13

import Data.Char
import Data.List


tenDigits :: String -> String
tenDigits xs = take 10 (show(foldr (+) (0) (map (fromIntegral.read) ys)))
             where ys = lines xs
             

main = do
      myFile <- readFile "numbers.txt"
      putStrLn (tenDigits myFile)
      
       



